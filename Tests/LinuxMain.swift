import XCTest

import SampleSwiftPackageTests

var tests = [XCTestCaseEntry]()
tests += SampleSwiftPackageTests.allTests()
XCTMain(tests)
